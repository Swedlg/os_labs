package com.company;

public class Main {

    public static void main(String[] args) {

        final Stack stack = new Stack();//Неизменяемый стек
        final SystemCore systemCore = new SystemCore(stack);//Неизменяемое системное ядро

        //Вызываем функцию выводящую в консоль все "системный вызовы" по индексам с описанием входных аргументов
        systemCore.printDescription();

        //Заполняем стек "системными вызовами"
        stack.push(0, new Object[] {12, 12});
        stack.push(0, new Object[] {12, "строка"});
        stack.push(1, new Object[] {"строка", 12.5});
        stack.push(2, new Object[] {12.5, 12});
        stack.push(3, new Object[] {"строка", "строка", "строка"});
        stack.push(4, new Object[] {12, 12, 12});
        stack.push(5, new Object[] {12, "строка"});

        System.out.println();

        //Вызываем "системные вызовы" если они есть в стеке. После вызова они удаляются из стека
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
        systemCore.systemCall();
    }
}
