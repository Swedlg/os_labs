package com.company;

public class Node {

    private Node nextNode;//Ссылка на нижний узел стека (тот что ниже этого узла)
    private int index;//Индекс (идентификатор) "системного вызова"
    private Object[] object;//Массив объектов (входных аргументов) "системного вызова"

    /**
     * Конструктор узла стека. Инициализирует поля класса
     */
    public Node(int index, Object[] object) {
        this.index = index;
        this.object = object;
    }

    /**
     * Этот метод вернет нижний узел стека
     */
    public Node getNextNode(){ return nextNode; }

    /**
     * Этот метод поменяет ссылку на нижний узел стека
     * @param nextNode Узел стека
     */
    public void setNextNode(Node nextNode){ this.nextNode = nextNode; }

    /**
     * Этот метод вернет индекс (идентификатор) "системного вызова"
     */
    public int getIndex(){ return index; }

    /**
     * Этот метод вернет массив объектов (входных аргументов) "системного вызова"
     */
    public Object[] getObject(){ return object; }
}
