package com.company;

public class Stack  {

    private Node top = null;//Указатель на самый верхний узел стека (последний помещенный в стек)

    /**
     * Этот метод положит новый узел на самый верх стека
     * @param index Индекс системного вызова
     * @param object Массив объектов (в данном случае Integer String или Double)
     */
    public void push(int index, Object[] object) {
        Node newNode = new Node(index, object);
        newNode.setNextNode(top);
        top = newNode;

        System.out.print("Добавили в стек системный вызов с индексом " + newNode.getIndex() + ": ");
        for (Object o : object) {
            System.out.print(o + " ");
        }
        System.out.println();
    }

    /**
     * Этот метод вернет верхний узел стека если стек не пустой и удалит самый верхний узел из стека
     */
    public Node pop(){
        if (top != null) {
            Node proxyTop = top;
            top = top.getNextNode();
            return proxyTop;
        }
        else {
            System.out.println("В стеке закончились системные вызовы!");
            return null;
        }
    }
}
