package com.company;

public class SystemCall {

    private int index;//Индекс (идентификатор) "системного вызова"
    private Object[] object;//Массив объектов (входных аргументов) "системного вызова"
    private String description;//Описание входных аргументов "системного вызова"

    /**
     * Конструктор "системного вызова". Инициализирует поля класса
     */
    public SystemCall(int index, Object[] objects, String description){
        this.index = index;
        this.object = objects;
        this.description = description;
    }

    /**
     * Этот метод вернет индекс (идентификатор) "системного вызова"
     */
    public int getIndex(){ return index; }

    /**
     * Этот метод вернет массив объектов (входных аргументов) "системного вызова"
     */
    public  Object[] getObject(){ return object; }

    /**
     * Этот метод вернкт описание входных аргументов "системного вызова"
     */
    public String getDescription(){ return description; }
}
