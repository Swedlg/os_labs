package com.company;

public class SystemCore {

    private final int countOfSystemCalls = 5;//Количество индексов (идентификаторов) "системных вызовов"
    private SystemCall[] systemCallDescriptions = new SystemCall[countOfSystemCalls];//Массив всех возможных системных вызовов
    private Stack stack;//Стек. Инициализируется в конструкторе

    /**
     * Конструктор ядра. Заполняет массив с описанием работы "системных вызовов" по каждому индексу (идентификатору)
     */
    public SystemCore(Stack stack) {
        this.stack = stack;

        systemCallDescriptions[0] = new SystemCall(0, new Object[] {1, ""}, "Целое число и строка");
        systemCallDescriptions[1] = new SystemCall(1, new Object[] {"", 1.5},"Строка и дробное число");
        systemCallDescriptions[2] = new SystemCall(2, new Object[] {1.5, 1},"Дробное число и целое число");
        systemCallDescriptions[3] = new SystemCall(3, new Object[] {"", "", ""},"Строка, строка и строка");
        systemCallDescriptions[4] = new SystemCall(4, new Object[] {1, 1, 1},"Целое число, целое число, целое число");
    }

    /**
     * Этот метод вызовет вывод в консоль описание (входные аргументы) всех "системных вызовов" по индексам (идентификаторам)
     */
    public void printDescription(){
        for (SystemCall call : systemCallDescriptions) {
            System.out.println("Индекс = " + call.getIndex() + ", Входные аргументы: " + call.getDescription());
        }
        System.out.println();
    }

    /**
     * Этот метод вызовет "системный вызов"
     */
    public void systemCall() {
        Node node = stack.pop();

        if(node != null) {
            if (node.getIndex() >= countOfSystemCalls) {
                System.out.println("Нет системного вызова с индексом " + node.getIndex() + "!");
                return;
            }

            if(node.getObject().length == this.systemCallDescriptions[node.getIndex()].getObject().length){

                for(int i = 0; i < node.getObject().length; i++){
                    if(node.getObject()[i].getClass() != this.systemCallDescriptions[node.getIndex()].getObject()[i].getClass()){
                        System.out.println("В системном вызове по индексу " + node.getIndex() + " были неправильно указаны аргументы!");
                        return;
                    }
                }
                System.out.print("Вызывается системный вызов по индексу " + node.getIndex() + ": ");
                for(int i = 0; i < node.getObject().length; i++){
                    System.out.print(node.getObject()[0] + " ");
                }
                System.out.println();
            }
        }
    }
}
