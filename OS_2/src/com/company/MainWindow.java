package com.company;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class MainWindow {

    private SystemCore systemCore;//Системное ядро
    private JFrame frame;//Главное окно

    public MainWindow() {
        initialize();
    }

    /**
     * Инициализация окна программы
     */
    private void initialize() {
        frame = new JFrame();
        frame.setTitle("Прыткин Тимофей Лаба 2");
        frame.setBounds(100, 100, 1483, 831);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        Panel panel = new Panel();
        panel.setBackground(Color.black);
        panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        panel.setBounds(10, 11, 1447, 707);
        frame.getContentPane().add(panel);

        JButton button_1 = new JButton("Запустить");
        button_1.addActionListener(arg0 -> {
            systemCore = new SystemCore(panel);

             for (int i = 0; i <  Math.random() * 2 + 1; i++) {
                systemCore.createProcesses();
            }
            System.out.println();

            systemCore.toPlan();
        });
        button_1.setBounds(10, 729, 1447, 52);
        button_1.setFocusable(false);
        frame.getContentPane().add(button_1);
    }

    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
        mainWindow.frame.setVisible(true);
    }
}
