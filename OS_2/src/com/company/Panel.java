package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Panel extends JPanel {

    ArrayList<Object[]> arrayList = new ArrayList<>();//Список данных о планировании процессов и потоков
    ArrayList<Object[]> markArray = new ArrayList<>();//Список данных о метках, где заканчивается работа программы

    /**
     * Добавление данных о потоке и процессе в список данных о планировании процессов и потоков
     * @param pid Идентификатор процесса
     * @param tid Идентификатор потока
     * @param timeOfOneIteration Время выполнения итерации
     */
    public void addThread(int pid, int tid, int timeOfOneIteration) {
        arrayList.add( new Object[] {pid, tid, timeOfOneIteration} );
        System.out.println(arrayList.get(arrayList.size() - 1)[0] + " " + arrayList.get(arrayList.size() - 1)[1] + " " + arrayList.get(arrayList.size() - 1)[2]);
    }

    /**
     * Отрисовка
     * @param g Полотно
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int tempWidth = 0;
        int tempHeight = 30;
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));

        for (int i = 0; i < 5; i++) {
            g2.drawLine(0, tempHeight + (150 * i) + 100, this.getWidth(), tempHeight + (150 * i) + 100);
        }

        for (Object[] objects : arrayList) {

            if ((int) objects[0] == 0) {
                g2.setColor(Color.GREEN);
            } else if ((int) objects[0] == 1) {
                g2.setColor(Color.RED);
            } else {
                g.setColor(Color.BLUE);
            }

            g2.drawLine(tempWidth * 10, ((int) objects[1]) * 30 + tempHeight, (tempWidth + (int) objects[2]) * 10, ((int) objects[1]) * 30 + tempHeight);
            tempWidth += (int) objects[2];

            if (tempWidth + 10 > this.getWidth() / 10) {
                tempWidth = 0;
                tempHeight += 150;
            }
        }

        g2.setColor(Color.MAGENTA);

        for (Object[] objects : markArray) {
            g2.drawLine((int) objects[0], (int) objects[1], (int) objects[2], (int) objects[3]);
        }
        g2.drawLine(tempWidth * 10, tempHeight - 40, tempWidth * 10, tempHeight + 90);
        markArray.add(new Object[] {tempWidth * 10, tempHeight - 40, tempWidth * 10, tempHeight + 90});
    }
}