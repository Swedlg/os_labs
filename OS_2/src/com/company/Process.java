package com.company;

import java.util.ArrayList;

public class Process {

    private ArrayList<Thread> threads = new ArrayList<>();//Список потоков
    private int pid;//Идентификатор процесса
    private int timeOfOneIteration;//Время одной итерации процесса
    private int processExecutionTime = 0;//Затраченное время выполнения процесса
    Panel panel;//Панель отрисовки

    public Process(int pid, Panel panel) {
        this.pid = pid;
        System.out.println("Создаем процесс. PID: " + pid);

        this.panel = panel;

        for (int i = 0; i < Math.random() * 2 + 1; i++) {
            this.createThread();
            processExecutionTime += threads.get(i).getThreadExecutionTime();
        }
    }

    /**
     * Метод возвращает затраченное время выполнения процесса
     */
    public int getProcessExecutionTime(){
        return processExecutionTime;
    }

    /**
     * Метод возвращает время одной итерации процесса
     */
    public int getTimeOfOneIteration(){
        return timeOfOneIteration;
    }

    /**
     * Метод производит вычет
     */
    public void minusProcessExecutionTime(){
        processExecutionTime -= timeOfOneIteration;
    }

    /**
     * Метод для создания нового потока
     */
    public void createThread(){
        threads.add(new Thread(threads.size(), pid, panel));
    }

    /**
     * Метод для планирования потоков
     */
    public void toPlan(){
        int temp = 0;
        while (true) {
            for (int i = 0; i < threads.size(); i++) {
                threads.get(i).start();
                temp += threads.get(i).getTimeOfOneIteration();
                threads.get(i).minusThreadExecutionTime();
                if (threads.get(i).getThreadExecutionTime() < 0){
                    threads.remove(threads.get(i));
                    i--;
                }
                //Максимальное время для потока
                int maximumTimeForThreads = 30;
                if (temp > maximumTimeForThreads) {
                    timeOfOneIteration = temp;
                    return;
                }
            }
            if(threads.size() == 0){
                timeOfOneIteration = temp;
                return;
            }
        }
    }

    /**
     * Метод для начала процесса
     */
    public void start(){
        System.out.println("Начинаем процесс. PID: " + pid);
        toPlan();
    }
}
