package com.company;

import java.util.ArrayList;

public class SystemCore {

    ArrayList<Process> processes = new ArrayList<>();//Список процессов
    Panel panel;//Панель отрисовки

    public SystemCore(Panel panel) {
        this.panel = panel;
    }

    /**
     * Метод для создания нового процесса
     */
    public void createProcesses(){
        processes.add(new Process(processes.size(), panel));
    }

    /**
     * Метод для планирования процессов
     */
    public void toPlan(){
        int temp = 0;
        while (true) {
            for (int i = 0; i < processes.size(); i++) {
                processes.get(i).start();
                temp += processes.get(i).getTimeOfOneIteration();
                processes.get(i).minusProcessExecutionTime();
                if (processes.get(i).getProcessExecutionTime() < 0){
                    processes.remove(processes.get(i));
                    i--;
                }
                //Максимальное время для процесса
                int maximumTimeForProcesses = 500;
                if (temp > maximumTimeForProcesses) {
                    return;
                }
            }
            if(processes.size() == 0){
                return;
            }
        }
    }
}
