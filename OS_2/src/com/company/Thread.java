package com.company;

public class Thread {

    private int tid;//Идентификатор потока
    private int pid;//Идентификатор процесса
    private int threadExecutionTime = (int) (Math.random() * 20) + 10;//Оставшееся время выполнения процесса
    private int timeOfOneIteration = (int) (Math.random() * 3) + 3;//Время одной итерации процесса
    Panel panel;//Панель отрисовки

    public Thread(int tid, int pid, Panel panel) {
        this.tid = tid;
        this.pid = pid;
        System.out.println("Создаем поток. TID: " + tid);

        this.panel = panel;
    }

    /**
     * Метод возвращает оставшееся время выполнения потока
     */
    public int getThreadExecutionTime(){
        return threadExecutionTime;
    }

    /**
     * Метод возвращает время одной итерации потока
     */
    public int getTimeOfOneIteration(){
        return timeOfOneIteration;
    }

    /**
     * Метод производит вычет
     */
    public void minusThreadExecutionTime(){
        threadExecutionTime -= timeOfOneIteration;
    }

    /**
     * Метод для начала потока
     */
    public void start(){
        System.out.println("Начинаем поток. TID: " + tid);
        System.out.println("Нужно времени для выполнения: " + threadExecutionTime);

        panel.addThread(pid, tid, timeOfOneIteration);
        panel.repaint();
    }
}
