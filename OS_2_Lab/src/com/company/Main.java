package com.company;

public class Main {

    public static void main(String[] args) {
        //Системное ядро
        SystemCore systemCore = new SystemCore();

        for (int i = 0; i <  Math.random() * 2 + 1; i++) {
            systemCore.createProcess();
        }
        System.out.println();

        systemCore.cloneProcessesThreads();//Клонировать для повторного теста

        System.out.println("\n\nЗатраченное время выполнения планировщика без прерываний: " + systemCore.toPlanProcessWithoutInterrupting() + "\n\n");
        System.out.println("\n\nЗатраченное время выполнения планировщика c прерываниями: " + systemCore.toPlanProcessWithInterrupting() + "\n\n");



    }
}
