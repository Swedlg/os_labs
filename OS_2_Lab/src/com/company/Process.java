package com.company;

import java.util.Random;

public class Process {

    //Идентификатор процесса
    private int pid;

    /**
     * Конструктор процесса
     * @param pid Идентификатор процесса
     */
    public Process(int pid,  boolean displayLabel) {
        this.pid = pid;
        if (displayLabel) {
            System.out.println("Создаем процесс. PID: " + pid);
        }
    }

    /**
     * Метод возвращает идентификатор процесса
     */
    public int getPID(){
        return pid;
    }

    /**
     * Метод создания потока
     */
    public Thread createThread(int threadsSize) {
        return new Thread(threadsSize, pid, (new Random()).nextBoolean(), true);
    }

    /**
     * Метод для начала процесса
     */
    public void start(){
        System.out.println("Начинаем процесс. PID: " + pid);
    }
}
