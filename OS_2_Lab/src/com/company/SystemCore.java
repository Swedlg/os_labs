package com.company;

import java.util.ArrayList;
import java.util.HashMap;

public class SystemCore {

    ArrayList<Process> processes = new ArrayList<>();//Список процессов
    HashMap<Integer, ArrayList<Thread>> mapThreads = new HashMap<>();//Хэш-мапа для потоков (ключ PID) значение список потоков

    ArrayList<Process> clonedProcesses = new ArrayList<>();//Список для склонированных процессов
    HashMap<Integer, ArrayList<Thread>> clonedMapThreads = new HashMap<>();//Хэш-мапа для склонированных потоков (ключ PID) значение список потоков

    //максимальное время на потоки (на один процесс)
    private int maxTimeOfThreads = 30;

    ArrayList<Thread> blockedThreads = new ArrayList<>();//Заблокирлованные потоки

    public void cloneProcessesThreads(){
        for (Process process : processes) {
            clonedProcesses.add(new Process(process.getPID(), false));
        }

        for (int i = 0; i < mapThreads.size(); i++) {

            ArrayList<Thread> threads = new ArrayList<>();
            for (int j = 0; j < mapThreads.get(i).size(); j++) {
                threads.add(mapThreads.get(i).get(j).cloneThread());
            }
            clonedMapThreads.put(i, threads);
        }
    }

    /**
     * Метод для создания нового процесса
     */
    public void createProcess() {
        processes.add(new Process(processes.size(), true));
        createThreads(processes.size() - 1);
    }

    /**
     * Метод для создания новых потоков
     * @param pid идентификатор процесса
     */
    public void createThreads(int pid) {
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < Math.random() * 2 + 1; i++) {
            threads.add(processes.get(processes.size() - 1).createThread(threads.size()));
        }
        mapThreads.put(pid, threads);
    }

    /**
     * Метод планирования процессов
     */
    public int toPlanProcessWithoutInterrupting() {

        System.out.println("\n\n" + "НАЧИНАЕМ ПЛАНИРОВАНИЕ ПРОЦЕССОВ БЕЗ ПРЕРЫВАНИЙ!" + "\n\n");

        int fullExecutionTime = 0;

        while (true) {

            for (int i = 0; i < processes.size(); i++) {

                processes.get(i).start();//Процесс ... начался

                int temp = toPlanThreadWithoutInterrupting(processes.get(i).getPID());//temp - затраченное на процесс время

                fullExecutionTime += temp;

                if(temp == 0){//Если затраченное на процесс вреямя равно
                    System.out.println("Удаляем процесс: " + processes.get(i).getPID());
                    processes.remove(i);//Удаляем этот процесс
                    break;
                }
            }

            if(processes.size() == 0){//Если размер списка процессов равен нулю
                return fullExecutionTime;
            }
        }
    }

    /**
     * Метожд планирования потоков
     * @param pid Идентификатор процесса
     * @return Время затраченное на потоки
     */
    public int toPlanThreadWithoutInterrupting(int pid) {

        int temp = 0;//temp - затраченное на процессы время
        while (true) {

            for (int i = 0; i < mapThreads.get(pid).size(); i++) {

                mapThreads.get(pid).get(i).start();//Запускаем поток

                temp += mapThreads.get(pid).get(i).runWithoutInterrupting();

                if(mapThreads.get(pid).get(i).getThreadExecutionTime() <= 0){//Если оставшееся время потока меньше либо равно нулю
                    System.out.println("Удаляем поток: " + mapThreads.get(pid).get(i).getTID());
                    mapThreads.get(pid).remove(i);//Удаляем поток
                    i--;
                }

                System.out.println("Затраченное на процесс время: " + temp + ". (Максимальное время итерации процесса " + maxTimeOfThreads + ")" + '\n');

                if(temp >= maxTimeOfThreads){//Если затраченное на процессы время превысило максимальное допустимое вресмя на один процесс
                    System.out.println("Выхожу из планировщика потоков" + '\n');
                    return temp;
                }
            }

            if(mapThreads.get(pid).size() == 0){//Если потоки у данного процесса закончились
                return temp;
            }
        }
    }

    /**
     * Метод планирования процессов
     */
    public int toPlanProcessWithInterrupting() {

        System.out.println("\n\n" + "НАЧИНАЕМ ПЛАНИРОВАНИЕ ПРОЦЕССОВ С ПРЕРЫВАНИЯМИ!" + "\n\n");

        int fullExecutionTime = 0;

        while (true) {

            for (int i = 0; i < clonedProcesses.size(); i++) {

                clonedProcesses.get(i).start();//Процесс ... начался

                int temp = toPlanThreadWithInterrupting(clonedProcesses.get(i).getPID());//temp - затраченное на процесс время

                fullExecutionTime += temp;

                if(temp == 0){//Если затраченное на процесс вреямя равно
                    System.out.println("Удаляем процесс: " + clonedProcesses.get(i).getPID());
                    clonedProcesses.remove(i);//Удаляем этот процесс
                    break;
                }
            }

            if(clonedProcesses.size() == 0){//Если размер списка процессов равен нулю
                return fullExecutionTime;
            }
        }
    }

    /**
     * Метожд планирования потоков
     * @param pid Идентификатор процесса
     * @return Время затраченное на потоки
     */
    public int toPlanThreadWithInterrupting(int pid) {

        int temp = 0;//temp - затраченное на процессы время
        while (true) {

            for (int i = 0; i < clonedMapThreads.get(pid).size(); i++) {

                if(blockedThreads.contains(clonedMapThreads.get(pid).get(i))){//Если этот процесс находится в заблокированных
                    if (clonedMapThreads.get(pid).get(i).getThreadExecutionTime() <= 0){
                        blockedThreads.remove(clonedMapThreads.get(pid).get(i));
                        System.out.println("Удаляем поток: " + clonedMapThreads.get(pid).get(i).getTID());
                        clonedMapThreads.get(pid).remove(i);
                        i--;
                    }
                    continue;//переходим к планировке следующих процессов
                }

                clonedMapThreads.get(pid).get(i).start();//Запускаем поток

                int resultOfRunning = clonedMapThreads.get(pid).get(i).runWithInterrupting();//Записываем результат запуска потока

                for (Thread blockedThread : blockedThreads) {//Тем временем заблокированные потоки ждут ввода-вывода
                    blockedThread.waitIO(resultOfRunning);
                }

                if(resultOfRunning == -1){//Если результат вернул -1 значит поток так и не дождался ввода-вывода
                    temp +=  clonedMapThreads.get(pid).get(i).getTimeOfOneIteration();
                    blockedThreads.add(clonedMapThreads.get(pid).get(i));//Добавляем этот поток в заблокированные
                } else if (resultOfRunning >= 0) {
                    temp += resultOfRunning;
                }

                if(clonedMapThreads.get(pid).get(i).getThreadExecutionTime() <= 0){//Если оставшееся время потока меньше либо равно нулю
                    System.out.println("Удаляем поток: " + clonedMapThreads.get(pid).get(i).getTID());
                    clonedMapThreads.get(pid).remove(i);//Удаляем поток
                    i--;
                }

                System.out.println("Затраченное на процесс время: " + temp + ". (Максимальное время итерации процесса " + maxTimeOfThreads + ")" + '\n');

                if(temp >= maxTimeOfThreads){//Если затраченное на процессы время превысило максимальное допустимое вресмя на один процесс
                    System.out.println("Выхожу из планировщика потоков" + '\n');
                    return temp;
                }
            }

            boolean existThreadWithoutBlocking = false;

            for (int i = 0; i < clonedMapThreads.get(pid).size(); i++) {//Ищем есть ли поток без блокировки
                if(!blockedThreads.contains(clonedMapThreads.get(pid).get(i))){
                    existThreadWithoutBlocking = true;
                }
            }

            if(clonedMapThreads.get(pid).size() == 0 || !existThreadWithoutBlocking){//Если потоки у данного процесса закончились или нет потоков без блокировки
                return temp;
            }
        }
    }
}
