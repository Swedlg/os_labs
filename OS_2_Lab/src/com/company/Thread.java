package com.company;

public class Thread  {

    //Идентификатор потока
    private int tid;

    //Идентификатор процесса
    private int pid;

    //Оставшееся время выполнения процесса
    private int threadExecutionTime = (int) ((Math.random() * 20) + 10);

    //Время одной итерации процесса
    private int timeOfOneIteration = (int) ((Math.random() * 7) + 3);

    //Есть ли у этого потока ввод/вывод
    private boolean hasInputOutput;

    //Время до отклика устройства ввода-вывода
    private int IOWaitingTime;

    /**
     * Конструктор потока
     * @param tid Идентификатор потока
     * @param pid Идентификатор процесса
     */
    public Thread(int tid, int pid, boolean hasInputOutput, boolean displayLabel) {
        this.tid = tid;
        this.pid = pid;
        this.hasInputOutput = hasInputOutput;
        if(hasInputOutput) {
            IOWaitingTime = (int) ((Math.random() * 20) + 10);
        }
        if(displayLabel){
            System.out.println("Создаем поток. TID: " + tid + (hasInputOutput ? " Есть ввод/вывод" : " Нет ввода/вывода") + ". Время выполнения " + threadExecutionTime + ". Время одной итерации " + timeOfOneIteration);
        }
    }

    /**
     * Клонирует поток
     * @return Копия потока
     */
    public Thread cloneThread(){
        Thread thread = new Thread(tid, pid, hasInputOutput, false);
        thread.cloneParameters(threadExecutionTime, timeOfOneIteration, IOWaitingTime);
        return thread;
    }

    /**
     * Метод возвращает оставшееся время выполнения потока или оставшееся время до вводы-вывода
     */
    public int getThreadExecutionTime(){

        if(!hasInputOutput){
            return threadExecutionTime;
        } else {
            return IOWaitingTime;
        }

    }

    /**
     * Метод возвращает время одной итерации потока
     */
    public int getTimeOfOneIteration(){
        return timeOfOneIteration;
    }

    /**
     * Запуск потока
     */
    public void start(){
        System.out.println("Начинаем поток. PID: " + pid + ", TID: " + tid);
        System.out.println("Нужно времени для выполнения: " + (hasInputOutput ? IOWaitingTime : threadExecutionTime));
    }

    /**
     * Возвращает идентийфикатор потока
     * @return идентификатор потока
     */
    public int getTID() {
        return tid;
    }

    /**
     * Возвращет есть ли у потока ввод/вывод
     * @return Есть ли у потока ввод/вывод
     */
    public boolean isHasInputOutput() {
        return hasInputOutput;
    }

    /**
     * Запуск потока без прерываний
     * @return затраченное время
     */
    public int runWithoutInterrupting(){
        if(hasInputOutput){//Если имеет ввод-вывод
            int spentTime = IOWaitingTime;
            IOWaitingTime = 0;
            return spentTime;
        } else {//Если не имеет ввод вывод
            threadExecutionTime -= timeOfOneIteration;
            return getTimeOfOneIteration();
        }
    }



    /**
     * Запуск потока с прерываниями
     * @return затраченное время
     */
    public int runWithInterrupting(){
        if(hasInputOutput){//Если имеет ввод-вывод

            if(IOWaitingTime > timeOfOneIteration){
                IOWaitingTime -= timeOfOneIteration;
                return -1;
            } else {
                int spentTime = IOWaitingTime;
                IOWaitingTime = 0;
                return spentTime;
            }

        } else {//Если не имеет ввод вывод
            threadExecutionTime -= timeOfOneIteration;
            return getTimeOfOneIteration();
        }
    }

    /**
     * Клонирует случайные параметры
     * @param threadExecutionTime Оставшееся время выполнения процесса
     * @param timeOfOneIteration Время одной итерации процесса
     * @param IOWaitingTime Время до отклика устройства ввода-вывода
     */
    public void cloneParameters(int threadExecutionTime, int timeOfOneIteration, int IOWaitingTime) {
        this.threadExecutionTime = threadExecutionTime;
        this.timeOfOneIteration = timeOfOneIteration;
        this.IOWaitingTime = IOWaitingTime;
    }

    /**
     * Уменьшение времени ожидания
     * @param time Время на которое уменьшается ожидание
     */
    public void waitIO(int time){
        IOWaitingTime -= time;
    }
}


