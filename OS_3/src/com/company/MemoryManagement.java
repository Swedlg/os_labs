package com.company;

import java.util.LinkedList;
import java.util.Queue;

public class MemoryManagement {

    //Оператативная память.
    private Node[] randomAccessMemory = new Node[16];

    //Список процессов.
    private LinkedList<Process> processes = new LinkedList<>();

    //Очередь страниц
    private Queue<Node> queue = new LinkedList();

    /**
     * Метод добавления процессов в список процессов
     */
    public void addProcesses(){
        for (int i = 0; i < 3; i++) {
            processes.add(new Process(i));
        }
    }





    public Node poolFromQueue(){
        if (queue.size() == 16){
            return null;
        }
        else{
            return null;
        }
    }

    public void addToQueue(Node node){
        if (queue.size() < 16){
            queue.add(node);
        }

    }







    /**
     * Метод запуска исполнения процессов
     */
    public void run(){

        int tempPage = 0;

        int j = 0;
        while(j < 10) {



            for (int i = 0; i < processes.size(); i++) {

                System.out.print("Процесс " + processes.get(i).getPid() + ". ");
                randomAccessMemory[tempPage] = processes.get(i).runTheReplacementAlgorithm(randomAccessMemory[tempPage]);


                if(randomAccessMemory[tempPage] != null){
                    System.out.println("Загрузили страницу " + randomAccessMemory[tempPage].getPageID()  + ". ");
                } else {
                    System.out.println("Не удалось загрузить страницу. Страницы кончились.");
                    tempPage--;


                    for (Node node : randomAccessMemory) {
                        if (node.getPid() == processes.get(i).getPid()) {
                            System.out.println("Удаляется процесс " + processes.get(i).getPid());
                            processes.remove(processes.get(i));
                            i--;
                            break;
                        }
                    }

                }

                tempPage++;
                if(tempPage >= randomAccessMemory.length){ tempPage = 0;}

            }

            j++;
        }
    }
}


