package com.company;

public class Node {

    private Page page;

    private int pageID;



    private int pid;

    private boolean callSign = false;

    private boolean doNotNeedAnymore = false;

    public Node(Page page, int pid, int pageID) {
        this.pid = pid;
        this.page = page;
        this.pageID = pageID;
    }

    public int getPageID() {
        return pageID;
    }

    public int getPid() {
        return pid;
    }

    public void setCallSign(boolean callSign) {
        this.callSign = callSign;
    }

    public boolean isCallSign() {
        return callSign;
    }

    public boolean isDoNotNeedAnymore() {
        return doNotNeedAnymore;
    }

    public void setDoNotNeedAnymore(boolean doNotNeedAnymore) {
        this.doNotNeedAnymore = doNotNeedAnymore;
    }
}
