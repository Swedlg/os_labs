package com.company;

import java.util.Random;

public class Page {

    Random randomGenerator = new Random();

    //Страница обычно имеет размер 4 КБ, её размер обезательно равен двойке в какой-либо степени
    private boolean[] bits = new boolean[32_768];

    public Page() {
        for (int i = 0; i < bits.length; i++) {
            bits[i] = randomGenerator.nextBoolean();
        }
    }
}

