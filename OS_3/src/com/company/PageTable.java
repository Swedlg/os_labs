package com.company;

import java.util.LinkedList;

public class PageTable {

    //Размер списка страниц
    private final int sizeOfListOfPages = (int) (Math.random() * 15) + 1;

    private int pid;

    //Массив страниц.
    private LinkedList<Node> listOfPages = new LinkedList();

    public PageTable(int pid) {
        this.pid = pid;
        System.out.println("Всего страниц в этом процессе: " + sizeOfListOfPages);
        for (int i = 0; i < sizeOfListOfPages; i++) {
            listOfPages.add(new Node(new Page(), pid, i));
            System.out.println("Создалась страница " + i + ". Её признак обращения false");
        }
    }

    public Node loadFromDisk(){
        if(!listOfPages.isEmpty()){
            Node tempNode = listOfPages.getFirst();
            listOfPages.removeFirst();

            System.out.println("На диске в этом процессе осталось " + listOfPages.size() + " страниц.");
            return tempNode;
        }
        else {
            return null;
        }
    }

    public void uploadToDisk(Node node){
        listOfPages.add(node);
    }
}
