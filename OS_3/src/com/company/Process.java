package com.company;

public class Process {

    private int pid;

    private PageTable pageTable;

    public Process(int pid) {
        this.pid = pid;
        System.out.print("Создается ппроцесс. PID " + pid + ". ");
        pageTable = new PageTable(pid);
    }

    public int getPid() {
        return pid;
    }

    /**
     * Метод запуска исполнения процесса
     */
    public Node runTheReplacementAlgorithm(Node node){


        if(node == null){
            System.out.print("Пустая страница. ");
            return pageTable.loadFromDisk();

        } else {
            return null;
        }


    }
}
