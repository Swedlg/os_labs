package com.company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class MemoryManagement {

    //Оператативная память.
    private Node[] randomAccessMemory = new Node[16];

    //Список процессов.
    private LinkedList<Process> processes = new LinkedList<>();

    //Очередь страниц
    private Queue<Node> queue = new LinkedList();

    //Таблица процессов
    private Map<Process, PageTable> pageTables = new HashMap();

    /**
     * Метод добавления процессов в список процессов
     */
    public void addProcessesToMemory(){
        for (int i = 0; i < 3; i++) {
            processes.add(new Process(i));
            pageTables.put(processes.get(i), processes.get(i).getPageTable());
        }
    }

    /**
     * Разместить страницу в оперативной памяти
     * @param node Узел
     */
    public void setToRandomAccessMemory(Node node){
        for (int i = 0; i < randomAccessMemory.length; i++) {
            if(randomAccessMemory[i] == null){

                if(node != null){
                    System.out.println("Добавлеям в оперативную память страницу " + node.getPageID() + " процесса " + node.getPid());
                }

                randomAccessMemory[i] = node;
                return;
            }
        }
    }

    /**
     * Удалить из оперативной памяти
     */
    public void removeFromRandomAccessMemory(Node node){
        for (int i = 0; i < randomAccessMemory.length; i++) {
            if(randomAccessMemory[i] == node){
                System.out.println("Удалеяем из оперативной памяти страницу " + node.getPageID() + " процесса " + node.getPid());
                randomAccessMemory[i] = null;
                return;
            }
        }
    }

    /**
     * Вернуть страницу в таблицу страниц ее процесса
     * @param node Узел
     */
    public void backPageFromQueueToPageTable(Node node){
        for (int i = 0; i < processes.size(); i++) {
            if(node.getPid() == processes.get(i).getPid()){
                setPageIntoPageTable(pageTables.get(processes.get(i)), node);
                return;
            }
        }
    }

    /**
     * Добавление страницы в очередь
     * @param node Узел
     */
    public void addToQueue(Node node){

        if (queue.size() < 16){//
            queue.add(node);
            setToRandomAccessMemory(node);
        } else {
            Node tempNode;

            while(true){

                tempNode = queue.poll();
                tempNode.setCallSign();

                if(tempNode.isCallSign()){
                    queue.add(tempNode);
                }
                else {
                    queue.add(node);
                    removeFromRandomAccessMemory(tempNode);
                    setToRandomAccessMemory(node);
                    backPageFromQueueToPageTable(tempNode);
                    return;
                }
            }
        }
    }

    /**
     * Отправка страницы на диск
     * @param pageTable
     * @param node
     */
    public void setPageIntoPageTable(PageTable pageTable, Node node){
        pageTable.uploadToDisk(node);
    }

    /**
     * Загрузка страницы с диска
     * @param pageTable
     */
    public Node getNextPage(PageTable pageTable){
        System.out.print("Выгружаем страницу с диска. ");
        return pageTable.loadFromDisk();
    }

    /**
     * Метод запуска исполнения процессов
     */
    public void run(){
        while(!processes.isEmpty()) {

            for (int i = 0; i < processes.size(); i++) {
                System.out.print("Процесс " + processes.get(i).getPid() + ". ");

                Node tempNode = getNextPage(pageTables.get(processes.get(i)));

                if (tempNode != null) {
                    addToQueue(tempNode);
                } else {
                    System.out.println("Не удалось загрузить страницу. Страницы кончились.");

                    tempNode = queue.poll();
                    removeFromRandomAccessMemory(tempNode);

                    boolean noPagesThisProcess = true;
                    for (Node node : randomAccessMemory) {
                        if(node != null) {
                            if (node.getPid() == processes.get(i).getPid()) {
                                noPagesThisProcess = false;
                            }
                        }
                    }

                    if(noPagesThisProcess) {
                        System.out.println("Удаляется процесс " + processes.get(i).getPid());
                        processes.remove(processes.get(i));
                        break;
                    }
                }
            }
        }
    }
}


