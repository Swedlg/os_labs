package com.company;

import java.util.Random;

public class Node {

    private Page page;

    private int pageID;

    private Integer pid;

    private boolean callSign = false;

    public Node(Page page, int pid, int pageID) {
        this.pid = pid;
        this.page = page;
        this.pageID = pageID;
    }

    public int getPageID() {
        return pageID;
    }

    public Integer getPid() {
        return pid;
    }

    public void setCallSign() {
        Random randomGenerator = new Random();
        this.callSign = randomGenerator.nextBoolean();
        System.out.println("Установили признак обращения страницы " + pageID + " потока " + pid + " на " + callSign + ". ");
    }

    public boolean isCallSign() {
        return callSign;
    }

}
