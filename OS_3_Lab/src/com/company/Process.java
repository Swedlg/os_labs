package com.company;

public class Process {

    private int pid;

    private PageTable pageTable;

    public Process(int pid) {
        this.pid = pid;
        System.out.print("Создается ппроцесс. PID " + pid + ". ");
        pageTable = new PageTable(pid);
    }

    public int getPid() {
        return pid;
    }

    public PageTable getPageTable() {
        return pageTable;
    }
}
