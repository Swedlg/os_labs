package com.company;

public class FileCluster {

    //Пустое ли это кластер на диске (Можно ли в него записывать)
    private boolean isEmpty;

    //Номер кластера на диске
    private int nextClusterNumberOnDisk;

    //Тип выделения (0-серый, 1-зеленый, 2-красный, 3-синий)
    private int typeOfSelection = 0;

    //Указатель на слудующий кластер
    private FileCluster nextCluster;

    /**
     * Конструктор кластера
     * @param needClusters Количество кластеров которое еще нужно создать
     * @param isEmpty Нужно ли помечать кластеры пустыми (при инициализации диска все кластеры помечаются пустыми)
     */
    public FileCluster(int needClusters, boolean isEmpty) {
        this.isEmpty = isEmpty;

        if (needClusters > 0) {
            System.out.println("Создаем кластер файла. Нужно еще кластеров " + needClusters);
            nextCluster = new FileCluster(needClusters - 1, false);
        }
    }

    /**
     * Возвращает значение пустой ли кластре
     * @return Пустой ли кластер
     */
    public boolean isEmpty() {
        return isEmpty;
    }

    /**
     * Возвращает ссылку на следующий кластер
     * @return Ссылка на следующий кластер
     */
    public FileCluster getNextCluster() {
        return nextCluster;
    }

    /**
     * Устанавливает кластеру номер кластера на диске
     * @param nextClusterNumberOnDisk Номер кластера на диске
     */
    public void setNextClusterNumberOnDisk(int nextClusterNumberOnDisk) {
        this.nextClusterNumberOnDisk = nextClusterNumberOnDisk;
    }

    /**
     * Получить тип типа выбора
     * @return Тип типа выбора
     */
    public int getTypeOfSelection() {
        return typeOfSelection;
    }

    /**
     * Установить тип типа выбора
     * @param typeOfSelection Тип типа выбора
     */
    public void setTypeOfSelection(int typeOfSelection) {
        if(typeOfSelection == 0){
           setEmpty(true);
        }
        this.typeOfSelection = typeOfSelection;
        if(nextCluster != null){
            nextCluster.setTypeOfSelection(typeOfSelection);
        }
    }

    /**
     * Устанавливает значение пустой ли кластер
     * @param empty Пустой ли кластер
     */
    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }
}
