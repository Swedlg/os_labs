package com.company;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.tree.*;
import java.awt.*;

public class FileSystem {

    //Диск
    private Disc disc;

    //Дерево (файловый менеджер)
    private JTree tree;


    private Methods methods;

    //Фрейм и разные кнопочки
    private JFrame frame;
    private JTextField textField;
    private JComboBox<String> comboBox;
    private JButton buttonCopy;
    private JButton buttonCopyTo;
    private JButton buttonReplace;
    private JButton buttonReplaceInto;

    //Нобор переменных для JComboBox
    private String[] items = {"jpg", "png", "wav", "txt"};

    //Выбранный узел (На который мы щелкнули на JTree)
    private DefaultMutableTreeNode selectedNode;

    //Узел который мы хотим добавить в JTree
    private DefaultMutableTreeNode newNode;

    /**
     * Начало программы
     * @param args Какие-то входные аргументы
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                FileSystem fileSystem = new FileSystem();
                fileSystem.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Конструктор класса Main вызывает инициализацию фрейма
     */
    public FileSystem() {
        initializeFrame();
    }


    /**
     * Инициализация фрейма
     */
    private void initializeFrame() {
        frame = new JFrame("Прыткин ПИбд-22 Лабораторная работа 4");


        int diskPartitionSize;
        while(true){
            diskPartitionSize = Integer.parseInt(JOptionPane.showInputDialog(frame, "Укажите размеры дискового раздела\nв КБ от 1024 до 8192,\n(рекомендуется 2048)"));
            if(diskPartitionSize <= 8192 && diskPartitionSize >= 1024){
                break;
            }
            JOptionPane.showMessageDialog(disc, "Ваше число не вхрдит в диапазон значений","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
        }

        int diskSectorSize;
        while(true){
            diskSectorSize = Integer.parseInt(JOptionPane.showInputDialog(frame, "Укажите размеры сектора диска\nв КБ от 2 до 8,\n(рекомендуется 4)"));
            if(diskSectorSize <= 8 && diskSectorSize >= 2){
                break;
            }
            JOptionPane.showMessageDialog(disc, "Ваше число не вхрдит в диапазон значений","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
        }


        frame.setBounds(100, 100, 1479, 622);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        disc = new Disc(diskPartitionSize, diskSectorSize);
        disc.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
        disc.setPreferredSize(new Dimension(1050, 1625));
        frame.getContentPane().add(disc);

        //Дерево (файловй менеджер)
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(new TreeNode("Корневая папка (Диск)", true));

        tree = new JTree(root);

        methods = new Methods(disc, tree);
        methods.initializeJTreeNodes(root);

        tree.setEditable(true);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
        model.reload();

        tree.addTreeSelectionListener(e -> {//Обработчик выделения узла дерева
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            if (node == null) return;
            Object nodeInfo = node.getUserObject ();
            System.out.println(nodeInfo.toString());
            methods.selectNode(node, 2, false);
        });
        tree.setCellRenderer(new MyTreeCellRenderer());

        //Поле для ввода имени файла или папки
        textField = new Placeholder("введитедлинноемнимоническоеимя");
        textField.setBounds(1091, 316, 268, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        //Выпадающая менюшка для выбора расширения файла
        comboBox = new JComboBox<>(items);
        comboBox.setBounds(1369, 316, 84, 20);
        comboBox.setFocusable(false);
        comboBox.setEditable(false);
        frame.getContentPane().add(comboBox);

        //Кнопка для создания файла
        JButton buttonCreate = new JButton("Создать файл");
        buttonCreate.addActionListener(arg0 -> {


                    selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                    TreeNode treeNode = methods.addToJTree(selectedNode, new TreeNode(textField.getText(), (String) comboBox.getSelectedItem(), false));
                    if(treeNode != null){
                        disc.addToDisc(treeNode);
                    }
                    disc.repaint();


                }
        );
        buttonCreate.setBounds(1091, 367, 174, 43);
        frame.getContentPane().add(buttonCreate);

        //Кнопка для создания папки
        JButton buttonCreateFolder = new JButton("Создать папку");
        buttonCreateFolder.addActionListener(e -> {
                    selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                    methods.addToJTree(selectedNode, new TreeNode(textField.getText(), true));
                    disc.repaint();
                }
        );
        buttonCreateFolder.setBounds(1279, 367, 174, 43);
        frame.getContentPane().add(buttonCreateFolder);

        //Кнопка для выбора какой файл или папку мы хотим копировать
        buttonCopy = new JButton("Копировать этот файл");
        buttonCopy.addActionListener(e -> {
            selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            int weightOfSelectedFolder = methods.calculateFolderWeight(selectedNode);
            if (weightOfSelectedFolder <= disc.getCountOfEmptySectors()){
                buttonCopy.setEnabled(false);
                buttonCopyTo.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(disc, "Размер этой папки слишком велик для копирования","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
            }
            disc.repaint();
        });
        buttonCopy.setBounds(1091, 421, 174, 43);
        buttonCopy.setEnabled(true);
        frame.getContentPane().add(buttonCopy);

        //Кнопка для выбора в какую папку мы хотим копировать выбранный нами файл или папку
        buttonCopyTo = new JButton("Копировать в эту папку");
        buttonCopyTo.addActionListener(e -> {
            buttonCopy.setEnabled(true);
            buttonCopyTo.setEnabled(false);
            newNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

            if(selectedNode == newNode){
                JOptionPane.showMessageDialog(disc, "Нельзя копировать папку в саму себя","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
            } else {
                methods.copyToJTree(selectedNode, newNode, true);
            }
            disc.repaint();
        });
        buttonCopyTo.setBounds(1279, 421, 174, 43);
        buttonCopyTo.setEnabled(false);
        frame.getContentPane().add(buttonCopyTo);

        //Кнопка для выбора какой файл или папку мы хотим переместить
        buttonReplace = new JButton("Переместить этот файл");
        buttonReplace.addActionListener(e -> {
            buttonReplace.setEnabled(false);
            buttonReplaceInto.setEnabled(true);
            selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            disc.repaint();
        });
        buttonReplace.setBounds(1091, 475, 174, 43);
        buttonReplace.setEnabled(true);
        frame.getContentPane().add(buttonReplace);

        //Кнопка для выбора в какую папку мы хотим переместить выбранный нами файл или папку
        buttonReplaceInto = new JButton("Переместить в эту папку");
        buttonReplaceInto.addActionListener(e -> {
            buttonReplace.setEnabled(true);
            buttonReplaceInto.setEnabled(false);
            newNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

            if(selectedNode == newNode){
                JOptionPane.showMessageDialog(disc, "Нельзя перемещать папку в саму себя","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
            } else {
                methods.replaceIntoJTree(selectedNode, newNode);
            }
            disc.repaint();
        });
        buttonReplaceInto.setBounds(1279, 475, 174, 43);
        buttonReplaceInto.setEnabled(false);
        frame.getContentPane().add(buttonReplaceInto);

        //Кнопка для удаления папки или файла
        JButton buttonRemove = new JButton("Удалить");
        buttonRemove.addActionListener(e -> {
            methods.removeFromJTree(true);
            System.out.print("");
            disc.repaint();
            }
        );
        buttonRemove.setBounds(1091, 529, 362, 43);
        frame.getContentPane().add(buttonRemove);

        //Возможность прокручивать дерево (файловый менеджер)
        JScrollPane treeView = new JScrollPane(tree);
        treeView.setBounds(1091, 11, 362, 294);
        frame.getContentPane().add(treeView);

        //Возможность прокручивать панель отрисовки
        JScrollPane scrollPane = new JScrollPane(disc);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBounds(10, 11, 1071, 561);
        frame.getContentPane().add(scrollPane);
    }
}