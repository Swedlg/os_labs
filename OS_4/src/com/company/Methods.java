package com.company;


import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class Methods{

    //Диск
    private Disc disc;

    //Дерево
    private JTree tree;

    /**
     * Конструктор
     * @param disc Диск
     * @param tree Дерево
     */
    public Methods(Disc disc, JTree tree) {
        this.disc = disc;
        this.tree = tree;
    }

    /**
     * Начальная инициализация JTree
     * @param root коренная папка (коренной узел)
     */
    public void initializeJTreeNodes(DefaultMutableTreeNode root) {
        DefaultMutableTreeNode tempFolder;
        DefaultMutableTreeNode tempFile;
        TreeNode treeNode;

        tempFolder = new DefaultMutableTreeNode(new TreeNode("Папка 1", true));
        root.add(tempFolder);

        treeNode = new TreeNode("Картинка 1", "jpg", false);
        tempFile = new DefaultMutableTreeNode(treeNode);
        tempFolder.add(tempFile);

        disc.addToDisc(treeNode);

        treeNode = new TreeNode("Картинка 2", "jpg", false);
        tempFile = new DefaultMutableTreeNode(treeNode);
        tempFolder.add(tempFile);
        disc.addToDisc(treeNode);

        treeNode = new TreeNode("Картинка 3", "png", false);
        tempFile = new DefaultMutableTreeNode(treeNode);
        tempFolder.add(tempFile);
        disc.addToDisc(treeNode);
        selectNode(tempFolder, 1, true);

        tempFolder = new DefaultMutableTreeNode(new TreeNode("Папка 2", true));
        root.add(tempFolder);

        treeNode = new TreeNode("Музыка для души", "wav", false);
        tempFile = new DefaultMutableTreeNode(treeNode);
        tempFolder.add(tempFile);
        disc.addToDisc(treeNode);

        treeNode = new TreeNode("Чит-коды для GTA SA", "txt", false);
        tempFile = new DefaultMutableTreeNode(treeNode);
        tempFolder.add(tempFile);
        disc.addToDisc(treeNode);

        selectNode(tempFolder, 1, true);
    }


    /**
     * Добавляет узел в дерево
     * @param file Файл
     */
    public TreeNode addToJTree( DefaultMutableTreeNode selectedNode, TreeNode file){

        if(selectedNode.isRoot() || ((TreeNode) selectedNode.getUserObject()).isFolder() ) {
            StringBuilder tempNameOfFile = new StringBuilder(file.getName());
            boolean isAlready = false;
            while (true) {
                //Проверяем нет ли уже такой папки или файла с таким же именем и расширением
                for (int i = 0; i < selectedNode.getChildCount(); i++) {
                    //Если это не папка, а "обычный файл" и имя и пасширение этого файла совпадают с новым добавляемым файлом
                    if (!(((TreeNode) ((DefaultMutableTreeNode) selectedNode.getChildAt(i)).getUserObject()).isFolder()) && ((TreeNode) ((DefaultMutableTreeNode) selectedNode.getChildAt(i)).getUserObject()).getName().contentEquals(tempNameOfFile) && ((TreeNode) ((DefaultMutableTreeNode) selectedNode.getChildAt(i)).getUserObject()).getFileExtension().equals(file.getFileExtension())) {
                        isAlready = true;
                    }
                    //Если это папка и имя совпадают с новой добовляемой папкой
                    if (((TreeNode) ((DefaultMutableTreeNode) selectedNode.getChildAt(i)).getUserObject()).isFolder() && ((TreeNode) ((DefaultMutableTreeNode) selectedNode.getChildAt(i)).getUserObject()).getName().contentEquals(tempNameOfFile)) {
                        isAlready = true;
                    }
                }

                if (!isAlready) {
                    break;
                } else {
                    tempNameOfFile.insert(0, "Новый - ");
                    isAlready = false;
                }
            }

            TreeNode newTreeNode;
            if(!file.isFolder()){
                newTreeNode = new TreeNode(tempNameOfFile.toString(), file.getFileExtension(), file.isFolder());
            } else {
                newTreeNode = new TreeNode(tempNameOfFile.toString(),  file.isFolder());
            }

            DefaultMutableTreeNode newJTreeNode = new DefaultMutableTreeNode(newTreeNode);
            selectedNode.add(newJTreeNode);
            DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
            model.reload();

            tree.scrollPathToVisible(new TreePath(newJTreeNode.getPath()));
            tree.setSelectionPath(new TreePath(newJTreeNode.getPath()));

            return newTreeNode;
        } else {
            JOptionPane.showMessageDialog(disc, "Вы пытаетесь добавить файл в ФАЙЛ","Что-то пошло не так", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    /**
     * Копирует узел
     * @param selectedNode Узел КОТОРЫЙ мы копируем
     * @param newNodeToCopy Узел В КОТОРЫЙ мы копируем
     */
    public void copyToJTree (DefaultMutableTreeNode selectedNode, DefaultMutableTreeNode newNodeToCopy, boolean createNewFile) {

        TreeNode file = (TreeNode) selectedNode.getUserObject();//Файл или каталог который мы хотим копировать
        StringBuilder tempNameOfFile = new StringBuilder(file.getName());
        boolean isAlready = false;
        while (true) {
            //Проверяем нет ли уже такой папки или файла с таким же именем и расширением
            for (int i = 0; i < newNodeToCopy.getChildCount(); i++) {
                //Если это не папка, а "обычный файл" и имя и пасширение этого файла совпадают с новым добавляемым файлом
                if (!(((TreeNode) ((DefaultMutableTreeNode) newNodeToCopy.getChildAt(i)).getUserObject()).isFolder()) && ((TreeNode) ((DefaultMutableTreeNode) newNodeToCopy.getChildAt(i)).getUserObject()).getName().contentEquals(tempNameOfFile) && ((TreeNode) ((DefaultMutableTreeNode) newNodeToCopy.getChildAt(i)).getUserObject()).getFileExtension().equals(file.getFileExtension())) {
                    isAlready = true;
                }
                //Если это папка и имя совпадают с новой добовляемой папкой
                if (((TreeNode) ((DefaultMutableTreeNode) newNodeToCopy.getChildAt(i)).getUserObject()).isFolder() && ((TreeNode) ((DefaultMutableTreeNode) newNodeToCopy.getChildAt(i)).getUserObject()).getName().contentEquals(tempNameOfFile)) {
                    isAlready = true;
                }
            }

            if (!isAlready) {
                break;
            } else {
                tempNameOfFile.insert(0, "Новый - ");
                isAlready = false;
            }
        }

        TreeNode newTreeNode;

        if(createNewFile){
            if(!file.isFolder()){
                newTreeNode = new TreeNode(selectedNode.getParent() == newNodeToCopy ? tempNameOfFile.toString()  + " - копия" : tempNameOfFile.toString(), file.getFileExtension(), file.isFolder());
            } else {
                newTreeNode = new TreeNode(selectedNode.getParent() == newNodeToCopy ? tempNameOfFile.toString()  + " - копия" : tempNameOfFile.toString(), file.isFolder());
            }
        } else {
            newTreeNode = (TreeNode) selectedNode.getUserObject();
        }

        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newTreeNode);//Новый узел в который мы скопируем
        newNodeToCopy.add(newNode);//Добавляем новый узел

        if(!file.isFolder() && createNewFile){
            disc.addToDisc(newTreeNode);
        }

        for (int i = 0; i < selectedNode.getChildCount(); i++) {//Копируем всех потомков
            copyToJTree((DefaultMutableTreeNode) selectedNode.getChildAt(i), newNode, createNewFile);
        }

        DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
        model.reload();
        tree.scrollPathToVisible(new TreePath(newNode.getPath()));

        selectNode(newNode, 3, true);
    }

    /**
     * Перемещает узел
     * @param selectedNode Узел КОТОРЫЙ мы копируем
     * @param newNode Узел В КОТОРЫЙ мы копируем
     */
    public void replaceIntoJTree (DefaultMutableTreeNode selectedNode, DefaultMutableTreeNode newNode){
        copyToJTree(selectedNode, newNode, false);
        tree.setSelectionPath(new TreePath(selectedNode.getPath()));
        removeFromJTree(false);
    }

    /**
     * Удадяет узел из дерева
     * @param removeFromDisc узел который мы хоти удалить
     */
    public void removeFromJTree(boolean removeFromDisc){
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if(selectedNode != tree.getModel().getRoot()) {

            if (removeFromDisc) {
                selectNode(selectedNode, 0, true);
            } else {
                selectNode(selectedNode, 3, true);
            }

            DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
            model.removeNodeFromParent(selectedNode);

            model.reload();
        }
    }

    /**
     * Посчитать вес папки
     * @param folder папка
     * @return Вес папки
     */
    public int calculateFolderWeight(DefaultMutableTreeNode folder){
        int result = 0;

        for (int i = 0; i < folder.getChildCount(); i++) {
            DefaultMutableTreeNode tempDefaultMutableTreeNode = (DefaultMutableTreeNode) folder.getChildAt(i);
            if (!((TreeNode) tempDefaultMutableTreeNode.getUserObject()).isFolder()){
                result += ((TreeNode) tempDefaultMutableTreeNode.getUserObject()).getFileSize();
            } else {
                result += calculateFolderWeight((DefaultMutableTreeNode) folder.getChildAt(i));
            }
        }

        System.out.println("Размер этой папки " + result);

        return result;
    }

    /**
     * Выделяем папку или файл
     * @param selectedNode Узел КОТОРЫЙ мы выделяем
     * @param typeOfSelection Тип выделения
     * @param re Рекурсивно ли вызывается функция
     */
    public void selectNode(DefaultMutableTreeNode selectedNode, int typeOfSelection, boolean re) {

        if(!re){
            disc.removeSelection();
        }

        //Добавляем новый тип выделенной папке или файлу
        if(!selectedNode.isRoot()) {

            if (!((TreeNode) selectedNode.getUserObject()).isFolder()) {//Если это файл выделяем только его
                ((TreeNode) selectedNode.getUserObject()).setTypeOfSelection(typeOfSelection);
            } else {
                for (int i = 0; i < selectedNode.getChildCount(); i++) {//Если это папка проходим по всем ее потомкам
                    DefaultMutableTreeNode tempDefaultMutableTreeNode = (DefaultMutableTreeNode) selectedNode.getChildAt(i);
                    selectNode(tempDefaultMutableTreeNode, typeOfSelection, true);
                }
            }
        }
        disc.repaint();
    }


}