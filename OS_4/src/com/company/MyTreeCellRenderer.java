package com.company;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

/**
 * Класс для изменения отображения иконок узлов дерева в завистимости от содержимого
 */
    class MyTreeCellRenderer extends DefaultTreeCellRenderer {

    /**
     * Выбор иконки для узла дерева
     * @param tree Дерево
     * @param value Узел дерева
     * @param sel Я без понятия что это
     * @param expanded Я без понятия что это
     * @param leaf Я без понятия что это
     * @param row Я без понятия что это
     * @param hasFocus Я без понятия что это
     * @return Возвращаем себя же
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        if (value instanceof DefaultMutableTreeNode){//Если узел является узлом дерева. А чем он еще может быть?
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

            if(node.isRoot()){//Если узел коренной
                ImageIcon imageIcon = new ImageIcon("Icons/disc.png"); // load the image to a imageIcon
                Image image = imageIcon.getImage(); // transform it
                Image newImage = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
                imageIcon = new ImageIcon(newImage);
                setIcon(imageIcon);
                return this;
            }

            if (node.getUserObject() instanceof TreeNode){//Если узел является файлом (или каталогом (папкой))

                TreeNode file = (TreeNode)node.getUserObject();

                if (file.isFolder()){//Если узел является каталогом (папкой)
                    ImageIcon imageIcon = new ImageIcon("Icons/folder.png"); // load the image to a imageIcon
                    Image image = imageIcon.getImage(); // transform it
                    Image newImage = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
                    imageIcon = new ImageIcon(newImage);
                    setIcon(imageIcon);

                    return this;
                } else {//Иначе он является файлом

                    String string = file.getFileExtension();
                    ImageIcon imageIcon;
                    switch (string){//В зависимости от расширения выбираем иконку
                        case "txt":
                            imageIcon = new ImageIcon("Icons/text.png");
                            break;
                        case "wav":
                            imageIcon = new ImageIcon("Icons/music.png");
                            break;
                        case "jpg":
                        case "png":
                            imageIcon = new ImageIcon("Icons/image.png");
                            break;
                        default:
                            setIcon(UIManager.getIcon("FileView.fileIcon"));
                            return this;
                    }

                    Image image = imageIcon.getImage();
                    Image newImage = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH);
                    imageIcon = new ImageIcon(newImage);
                    setIcon(imageIcon);
                }
            }
        }
        return this;
    }
}
