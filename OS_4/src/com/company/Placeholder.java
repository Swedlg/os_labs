package com.company;

import javax.swing.*;
import java.awt.*;

//Класс только для отрисовки начального текста в JTextField когда оно пустое
class Placeholder extends JTextField {
    String string;

    Placeholder(String str){
        super();
        string = str;
    }

    protected void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);
        if(getText().isEmpty()){
            Graphics2D g2 = (Graphics2D)g.create();
            g2.setBackground(Color.BLACK);
            g2.setColor(Color.gray);
            g2.drawString(string, 7, 14);
            g2.dispose();
        }
    }
}
