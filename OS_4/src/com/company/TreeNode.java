package com.company;

public class TreeNode {

    //Имя файла или папки
    private String name;

    //Расширение папки
    private String fileExtension;

    //Папка это или файл
    private boolean isFolder;

    //Рпазмер папки или файла
    private int fileSize;

    //Первый кластер файла
    private FileCluster firstFileCluster;

    /**
     * Конструктор для создания файла (не папки)
     * @param name Имя файла
     * @param fileExtension Расширение файла
     * @param isFolder Папка ли это (всегда должно быть false т.к. это конструктор файла)
     */
    public TreeNode(String name, String fileExtension, boolean isFolder) {
        this.name = name;
        this.fileExtension = fileExtension;
        this.isFolder = isFolder;

        System.out.println("Создаем файл с именем \"" + name + "\" и расширением \"" + fileExtension + "\"");

        setSize(fileExtension);
        firstFileCluster = new FileCluster(fileSize, false);
    }

    /**
     * Устанавливает размер файла в зависимости от его расширения
     * @param fileExtension Ркасширение файла
     */
    private void setSize(String fileExtension) {
        switch (fileExtension) {

            case "txt":
                fileSize = 2;
                break;
            case "wav":
                fileSize = 10;
                break;
            case "jpg":
                fileSize = 8;
                break;
            case "png":
                fileSize = 5;
                break;
            default:
                fileSize = 0;
                break;
        }
    }

    /**
     * Конструктор для создания папки
     * @param name Имя папки
     * @param isFolder Папка ли это (всегда должно быть true т.к. это конструктор папки)
     */
    public TreeNode(String name, boolean isFolder) {
        this.name = name;
        this.isFolder = isFolder;
    }

    /**
     * Возвращает имя файла и расширение или папки (у паки расширения нет)
     * @return имя файла или папки
     */
    public String toString(){
        if(isFolder){
            return name;
        }
        else{
            return name + "." + fileExtension;
        }
    }

    /**
     * Возвращает имя файла или папки
     * @return Имя файла или папки
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает расширение файла
     * @return Расширение файла
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * Проверяет является ли папкой этот файл
     * @return Является ли папкий этот файл
     */
    public boolean isFolder() {
        return isFolder;
    }

    /**
     * Возвращает размер файла
     * @return Размер файла
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * Возвращает первый кластер файла
     * @return Первый кластер файла
     */
    public FileCluster getFirstFileCluster() {
        return firstFileCluster;
    }

    /**
     * Устанавливает тип выделения
     * @param typeOfSelection Тип выделения
     */
    public void setTypeOfSelection(int typeOfSelection){
        System.out.println("Устанавливаем файлу \"" + name + "\" тип выделения " + typeOfSelection);
        firstFileCluster.setTypeOfSelection(typeOfSelection);
    }
}
